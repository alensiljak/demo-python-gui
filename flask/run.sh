#!/usr/bin/env bash

# Script for running the app in Termux.
# Make it executable. Put into ~/.shortcuts to be available to Termux:Widget.

# Run the server. Adjust the path to the start script. 
# Need to execute in parallel with the browser.
python app.py &

# Run the browser via intent.
am start --user 0 -a android.intent.action.VIEW -d "http://localhost:5000" &

wait 

echo Complete!