#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test the python bindings for webkit
"""
import sys
import gi

try:
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk, Gdk # , Gio
except:
    sys.exit("This script requires Gtk 3.0 or newer!")

try:
    gi.require_version('WebKit2', '4.0')
    from gi.repository import WebKit2
except:
    sys.exit("This script requires WebKit2 4.0 or newer!")

print("works")
