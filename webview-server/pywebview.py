#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a demo for [PyWebView](https://github.com/r0x0r/pywebview).
This can be used to create desktop versions of web apps, while Kivy covers Android. The
code base, however, should be the same, using web technologies.

For Python 3.7, the easiest way is to install the pythonnet binaries from 
http://www.lfd.uci.edu/%7Egohlke/pythonlibs/#pythonnet.

Then install pywebview.
"""


import webview

# Explicitly set the GUI library: qt, gtk, win32
#webview.config.gui = 'gtk'
#webview.config.gui = 'qt'
#webview.config.use_qt = True

webview.create_window("It works, Hamed!", "http://alensiljak.ml")
