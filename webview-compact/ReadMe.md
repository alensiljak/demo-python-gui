# PyWebView

PyWebView uses an instance of an available web client, depending on the platform, and displays it as a native app.

It can be used as a shell for a web server since it works on multiple platforms, including Windows, Linux, (and Android?), and provides an appearance of a GUI application.

The server and the client can be launched from the same Python script. One of them is started in a separately-created thread, while the other is in the main thread.

## Android

This does not seem to work on Android now.

`termux-open` and `termux-open-url` commands can be used in the shell scripts.
