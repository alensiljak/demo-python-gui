#!/usr/bin/env python3

import toga
from colosseum import CSS


class AboutMe(toga.App):
    """
    Simple about me app that aims to showcase
    Pybee Toga a little bit.
    Developed and tested on OSX. Might not work
    on other platforms.
    """
    
    def startup(self):
        """
        Sets up and creates main app
        and window instances
        """
        self.main_window = (toga.MainWindow(self.name, size=(200,200)))
        self.main_window.app = self
        
        # labels
        
        name = self.new_label('Name: Pablo Juan')
        
        email = self.new_label('Email: pryelluw@gmail.com')

        occupation = self.new_label('Occupation: Software Engineer')

        lang = self.new_label('Programmig language of choice: Python')

        learning = self.new_label('Currently learning: PyBee Toga, F#')

        # buttons
        
        read_blog_button = toga.Button(
        'Read my blog pablojuan.com',
        on_press=self.open_read_blog_window
        )
        
        read_blog_button.style.set(
            margin=15,
            max_width=50
            )

        view_twitter_button = toga.Button(
        'View my Twitter page @pryelluw',
        on_press=self.open_view_twitter_window
        )
        
        view_twitter_button.style.set(
            margin=15,
            max_width=50
            )

        # layout

        box = toga.Box(
            children=[
                name,
                email,
                occupation,
                lang,
                learning,
                view_twitter_button,
                read_blog_button
            ],
            style=CSS(flex_direction='column')
        )
        
        self.main_window.content = box
        
        # init child windows
        self.view_twitter_window()
        self.read_blog_window()

        # run it
        self.main_window.show()


    def read_blog_window(self, *args, **kwargs):
        """
        Setup the read blog window webview
        """
        
        self.read_blog_window = toga.Window(
            title='pablojuan.com!'
            )
        
        self.webview = toga.WebView(style=CSS(flex=1))
        self.webview.url = 'http://pablojuan.com'

        box = toga.Box(
            children=[self.webview],
            style=CSS(flex_direction='column')
            )

        self.read_blog_window.content = box
    
       
    def open_read_blog_window(self, *args, **kwargs):
        """
        Show the child window
        """  
        self.read_blog_window.show()


    def view_twitter_window(self, *args, **kwargs):
        """
        Setup the view twitter window
        """
        self.read_twitter_window = toga.Window(
            title='Twitter feed of: @pryelluw',
            size=(1024, 780)
            )
        
        self.webview = toga.WebView(style=CSS(flex=1))
        self.webview.url = 'https://twitter.com/pryelluw'

        box = toga.Box(
            children=[self.webview],
            style=CSS(flex_direction='column')
            )

        self.read_twitter_window.content = box


    def open_view_twitter_window(self, *args, **kwargs):
        """
        Open the twitter page on a new window
        """  
        self.read_twitter_window.show()


    def new_label(self, text):
        """
        A label factory to reduce
        repetitive code.
        """
        new_label = toga.Label(text)
        new_label.style.set(margin=15)
        
        return new_label
        

if __name__ == '__main__':

    app = AboutMe(
        'About me: Pablo Juan',
        'org.yelluw.pablojuan.com'
        )
    app.main_loop() 