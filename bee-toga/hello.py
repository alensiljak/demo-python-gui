"""
Sample hello world from https://toga.readthedocs.io/en/latest/tutorial/tutorial-0.html
This is using 0.3.0-dev11 version of Toga.
Pay attention to the version numbers, as the previous tutorial will *not* work:
https://toga.readthedocs.io/en/v0.2.15/tutorial/tutorial-0.html
"""

import toga


def button_handler(widget):
    print("hello")


def build(app):
    box = toga.Box()

    button = toga.Button('Hello world', on_press=button_handler)
    button.style.padding = 50
    button.style.flex = 1
    box.add(button)

    return box


def main():
    return toga.App('First App', 'org.pybee.helloworld', startup=build)


if __name__ == '__main__':
    main().main_loop()