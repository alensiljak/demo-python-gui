# Run these commands manually

# Activate python virtual environment
source venv/bin/activate
# deactivate

# Add android studio to path.
# https://briefcase.readthedocs.io/en/latest/background/getting-started.html

export PATH=$PATH:/home/alen/lib/android-sdk/tools:/home/alen/lib/android-sdk/platform-tools:/home/alen/lib/android-sdk/tools/bin
# env | grep PATH
