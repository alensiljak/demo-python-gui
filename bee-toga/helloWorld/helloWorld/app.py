
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW


class HelloWorld(toga.App):
    def startup(self):
        # Create a main window with a name matching the app
        self.main_window = toga.MainWindow(title=self.name)

        # Create a main content box
        #main_box = toga.Box()
        main_box = self.build()

        # Add the content on the main window
        self.main_window.content = main_box

        # Show the main window
        self.main_window.show()

    def button_handler(self, widget):
        print("hello")

    def build(self):
        box = toga.Box()

        button = toga.Button('Hello world', on_press=self.button_handler)
        button.style.padding = 50
        button.style.flex = 1
        box.add(button)

        return box


def main():
    return HelloWorld('Hello World', 'ml.alensiljak.python.helloWorld')
