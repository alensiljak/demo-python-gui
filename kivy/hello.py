"""
Hello World example from https://kivy.org/doc/stable/guide/basic.html#quickstart

On Windows, add "%LocalAppData%\Programs\Python\Python36\share\sdl2\bin" to path.
"""

import kivy
kivy.require('1.10.1') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label


class MyApp(App):

    def build(self):
        return Label(text='Hello world')


if __name__ == '__main__':
    MyApp().run()