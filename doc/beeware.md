# BeeWare

[BeeWare](https://pybee.org/) is a collection of tools allowing building native applications using Python. 

It contains [Toga](https://pybee.org/project/projects/libraries/toga/) library, a set of widgets that are rendered native on the respective platform.
Toga supports FlexBox. See the [FlexBox tutorial](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) for more info.

[Briefcase](https://pybee.org/project/projects/tools/briefcase/) is a tool that builds a native application from the sources.

## Installation

### Toga

Follow the instructions in the [Your first Toga app](https://toga.readthedocs.io/en/latest/tutorial/tutorial-0.html) tutorial.

In brief, create a virtual environment
Linux:

```
$ python3 -m venv venv
$ source venv/bin/activate
```

Windows:
```
virtualenv venv
activate_venv.bat
```

To exit the virtual environment later, use `deactivate`.

Install pycparser (required for pythonnet) via pip.

Set up PythonNet from Visual Studio's Developer Command Prompt. Use the latest version from GitHub.

```
pip install git+https://github.com/pythonnet/pythonnet
```

Install Toga:

```
(venv) $ pip install --pre toga
```
or install the following packages manually: [to be listed]

and, finally, run the Toga app (as a module!):

```
$ python -m helloworld
```

Note that Toga apps must be executed as modules, i.e. `python -m helloworld`.

#### Troubleshooting

You may run into the issue where gi (python GObject) is not available in virtual environment. See [details](https://github.com/pybee/toga/issues/621).

Link the system gi package with

```
pip install vext
pip install vext.gi
```

[Source](https://stackoverflow.0com/questions/39214803/how-to-install-python-gi-module-in-virtual-environment#43817335)

and make sure the versions of Toga components match.

On Linux, latest pycairo can be installed with 
`pip install --user git+https://github.com/pygobject/pycairo.git`
This requires python3-cairo-devel and gcc packages on openSuse.


### Briefcase

Briefcase is used to package apps for different platforms.

With the virtual environment in place, install Briefcase:

`pip install briefcase cookiecutter`

See [Tutorial](https://briefcase.readthedocs.io/en/latest/tutorial/tutorial-0.html) for more info. Install cookiecutter for the project templates.

Package and run the app by running

`python setup.py <platform> -s` 

Available platforms:

- linux
- windows
- android
- django

Parameters that can be added are `-s` or `--start`, and `-b` or `--build`.

To compile for Android, open the generated project in Android Studio. The Toga version (actually Voc) required for Android is 0.2.15.

For Django target, install Python packages `django environ`.

## Resources

- [ToDo App sample](https://github.com/eliasdorneles/todoapp-voc)
