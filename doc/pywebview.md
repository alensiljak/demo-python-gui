# WebView

Functionality is provided by `pywebview` library. It uses different components on different platforms.

## Front-End

This option uses a web client for the UI. The UI is done in HTML/CSS/JS. It can interact with backend Python code by contacting a web server or directly via the bridging API. 

## Backend: Server Option

The backend server can be served by any web/HTTP server. `Flask` is a good option.

## Backend: Integrated Option

Communicating directly with the backend code is also possible using the JS/Python bridge. Javascript in the HTML view can call Python methods directly and receive the data from functions. Python code can load different HTML pages into the web view component.

## Packaging

### Windows

Freezing the Python app should work.

### Android 

Packaging for Android needs to be investigated. `Python-for-Android` should work.

## Links

- [Sample](https://github.com/r0x0r/pywebview/blob/master/examples/flask_app/src/backend/main.py) on how to run Flask server and webview: 
- [Py-JS Interaction](https://github.com/r0x0r/pywebview/blob/master/examples/js_api.py)
