'''
Run the remi app and a web client.
This does not work!!!
'''
import logging
import webview
from threading import Thread, Lock
from time import sleep
import remi.gui as gui
from remi import start, App

# Configuration
server_ip: str = "127.0.0.1"
server_port: int = 8080
################

# This might be required.
server_lock = Lock()

logger = logging.getLogger(__name__)


class MyApp(App):
    def __init__(self, *args):
        super(MyApp, self).__init__(*args)

    def main(self):
        lbl = gui.Label("Hello world!", width=100, height=30)

        # return of the root widget
        return lbl


def url_ok(url, port):
    """ Check if the server has started """
    from http.client import HTTPConnection

    try:
        conn = HTTPConnection(url, port)
        conn.request("GET", "/")
        r = conn.getresponse()
        return r.status == 200
    except:
        logger.exception("Server not started")
        return False

def run_server():
    #start (MyApp)
    start(MyApp,address=server_ip, port=server_port, 
        multiple_instance=False, enable_file_cache=True, update_interval=0.1, 
        start_browser=True)


def main():
    """ entry point for the setup """
    logger.debug("Starting server")
    t = Thread(target=run_server)
    t.daemon = True
    t.start()
    logger.debug("Checking server")

    while not url_ok(server_ip, server_port):
        sleep(0.1)

    logger.debug("Server started")
    # webview.config.use_qt = True
    #webview.create_window("Alive!", url=f"http://{server_ip}:{server_port}", min_size=(1024, 768))


if __name__ == '__main__':
    main()
